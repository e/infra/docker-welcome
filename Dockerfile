FROM jekyll/jekyll:latest AS build
WORKDIR /tmp
COPY frontend/ /tmp
COPY --chown=jekyll:jekyll Gemfile Gemfile.lock /tmp/
RUN jekyll build -d ./build && rm /tmp/Gemfile*

FROM php:7.4-apache as server
RUN a2enmod rewrite && a2enmod env && a2enmod remoteip
ARG DOMAIN=murena.io
ARG TRUSTED_PROXY1=front1-proxy
ARG TRUSTED_PROXY2=front2-proxy
LABEL maintainer="dev@murena.io"

# configure the remoteip module
RUN echo "RemoteIPHeader X-Forwarded-For" > /etc/apache2/conf-available/remoteip.conf
RUN echo "RemoteIPTrustedProxy $TRUSTED_PROXY1 $TRUSTED_PROXY2" >> /etc/apache2/conf-available/remoteip.conf
RUN ln -s /etc/apache2/conf-available/remoteip.conf  /etc/apache2/conf-enabled/remoteip.conf

# replace %h = host always pointing to haproxy ip by %a in LogFormat to get real users ip
# as per https://www.globo.tech/learning-center/x-forwarded-for-ip-apache-web-server/
RUN sed -i 's/"%h/"%a/g' /etc/apache2/apache2.conf
RUN sed -i 's/ServerTokens OS/ServerTokens Prod/g' /etc/apache2/conf-enabled/security.conf
RUN sed -i 's/ServerSignature On/ServerSignature Off/g' /etc/apache2/conf-enabled/security.conf

# Use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN echo 'error_log = /var/log/errors.log' >> "$PHP_INI_DIR/php.ini"

# copy composer executable from official Docker image
# https://hub.docker.com/_/composer
COPY --from=composer:2.0 /usr/bin/composer /usr/bin/composer
RUN apt-get update && apt-get install -y libyaml-dev nano libpng-dev libfreetype6-dev libssl-dev  libjpeg62-turbo-dev && apt-get install -y --no-install-recommends git unzip
 # these params are recommended for installing untrusted extensions
 # https://getcomposer.org/doc/faqs/how-to-install-untrusted-packages-safely.md
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install mysqli
RUN apt-get install libldap2-dev -y && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
    docker-php-ext-install ldap
RUN COMPOSER_ALLOW_SUPERUSER=1 composer require --no-plugins --no-scripts directorytree/ldaprecord pear/mail pear/net_smtp pear/auth_sasl pear/mail_mime phpseclib/phpseclib:~3.0 curl/curl sendgrid/sendgrid
RUN apt-get remove -y git unzip
RUN rm -rf /var/lib/apt/lists/* && rm /usr/bin/composer
RUN chown -R www-data:www-data /var/www/html/vendor/ /var/www/html/composer.lock /var/www/html/composer.json
RUN pecl install yaml && echo "extension=yaml.so" > /usr/local/etc/php/conf.d/ext-yaml.ini

RUN mkdir /var/accounts/
RUN touch /var/accounts/auth.file /var/accounts/auth.file.done
RUN chown -R www-data:www-data /var/accounts

COPY --chown=www-data:www-data --from=build /tmp/build/ /var/www/html/
COPY --chown=www-data:www-data /private /var/private/
COPY htdocs/.htaccess /var/www/html/.htaccess
RUN chmod u+x /var/private/generate-signup-link.sh

COPY --chown=www-data:www-data htdocs /var/www/html/

RUN cd /var/www/html && ln -s ./ signup
