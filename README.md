Building
--------

When building image you can set --build.arg DOMAIN=yourdomain.tld to override
its default value that is DOMAIN=welcome.murena.io

If you want to avoid building you can also use the default image and set ENV var
IS_WELCOME=true and specify ENV var DOMAINS=welcome.yourdomain.com and DOMAIN=yourdomain.com

Example docker build + restart command:

```
docker build -t registry.gitlab.e.foundation/e/infra/docker-welcome:yourtag . \ 
&& docker stop welcome \ 
&& docker rm welcome \ 
&& docker run --publish 8080:80 -d --name welcome registry.gitlab.e.foundation/e/infra/docker-welcome:yourtag
```

# Account creation

The account creation has a captcha built-in. If you wish to use any tool to bypass this captcha, you need to set the ENV variable `BYPASS_CAPTCHA_TOKEN` with any token/hash that you want. Then, when sending the request to `process_email_invite.php` you need to set the GET parameter `bypass_captcha_token`;

## Murena Cloud and self-host

To be able to create an account to a Murena Cloud instance, you need to set the ENV variable `ECLOUD_ACCOUNTS_SECRET`

## Gitlab

To be able to create a gitlab account, you need to set the ENV variables `GITLAB_TOKEN` and `GITLAB_URL`.

For local development, `GITLAB_URL` can be http://localhost/ (don't forget to run an instance of gitlab). `GITLAB_TOKEN` can be generated on the gitlab instance. Both instances need to be on the **same docker network**.

## E solutions Shop (WordPress)
To be able to create an account on WP, set the ENV variables:
1. `E_SHOP_URL`, ex: "http://localhost:1080/"
1. `E_SHOP_USERNAME` with the privileged user account able to create other accounts, ex: "root"
1. `E_SHOP_APP_PASS` the user application password created as described in [here](https://make.wordpress.org/core/2020/11/05/application-passwords-integration-guide/)
1. `E_SHOP_ENABLE_REWARDS: true/false` set this to true to enable referral signup rewards 

Usage
-----

See https://gitlab.e.foundation/e/infra/ecloud-selfhosting for a working setup with
docker-compose.

To sign up, a user needs to have an authentication secret and authentication
email. With the docker-compose file mentioned above, these can be set in 
`/mnt/docker/accounts/auth.file` in the following format:
```
user@email.com:password
```
Then, the user can sign up with the following URL:
```
https://domain.io/signup/?authmail=user@email.com&authsecret=password
```

### Email Invite flow
```mermaid
flowchart TD
    A[User] --> |1. Request email invite through form| B{Welcome server}
    B --> |2. Generate secret code and create entry for user's email and code| C[(auth.file)]
    B --> |3. Send email invite with account creation link| A
```

### Account Creation flow
```mermaid
flowchart TD
    A[User] --> |1. Create account from invite link| B{Welcome server}
    B --> |2. Use postfixadmin to create mail account| C[Mailserver]
    B --> |3. Set quota and recovery email through OCS API| D[Murena Cloud server]
    B --> |4. Create entry for user's email, code and Murena username| E[(auth.file.done)]
    B --> |5. Send user welcome email| A
```
# To use with LDAP backend

- Add the following environment variables to use welcome with LDAP backend
    - `LDAP_HOSTS`, `LDAP_PORT`, `LDAP_ADMIN_DN`, `LDAP_ADMIN_PASSWORD`, `LDAP_USERS_BASE_DN`, `LDAP_BACKEND_ENABLED`

# SendGrid integration
To be able to use [SendGrid](https://sendgrid.com/) as the email sender, you need to set the following ENV variables:
- `SENDGRID_API_KEY`, something like: "SG.r_l1Ss0aRAB8..."
- `SENDGRID_EMAIL_FROM`, a valid sender email validated on SendGrid
- `SENDGRID_TEMPLATE_ID_EMAIL_CONFIRMATION_XX`, a template id of the email confirmation body to be used where XX is a valid language code (EN, FR, ...). Also created on SendGrid dashboard. ex: "d-47948c4b..."
- `SENDGRID_TEMPLATE_ID_ACCOUNT_CREATED_XX`, a template id of the email sent when the account was created where XX is a valid language code (EN, FR, ...). Also created on SendGrid dashboard. ex: "d-87143d1c..."

# License

The project is licensed under [AGPL](LICENSE).
