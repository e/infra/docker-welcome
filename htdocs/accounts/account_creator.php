<?php
require_once('validated_data.php');

interface AccountCreator
{
    /*
    * Validates that the account can be created with the input data
    */ 
    public function validateData(object $userData): ValidatedData;
    /*
    * Tries to create the account. Throws an exception if it fails.
    */
    public function tryToCreate(object $userData);
}
