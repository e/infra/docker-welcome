<?php
require 'vendor/autoload.php';
require_once('language.php');
require_once('helpers.php');

class BaseEcloudAccountCreator implements AccountCreator
{
    private string $ecloudUrl;
    private string $ecloudAccountsApiUrl;
    private string $commonApiUrl;
    protected int $quota = 1024; # Quota in MB
    protected bool $usernameIsEmail = true;

    public function __construct(string $ecloudUrl)
    {
        $this->ecloudUrl = endsWith($ecloudUrl, "/") ? $ecloudUrl : $ecloudUrl . "/";
        $this->ecloudAccountsApiUrl = $this->ecloudUrl . 'apps/ecloud-accounts/api/';

        $this->commonApiUrl = getenv('COMMON_SERVICES_URL');
        $this->commonApiUrl = endsWith($this->commonApiUrl, '/') ? $this->commonApiUrl : $this->commonApiUrl . '/';

        $quota = getenv('CLOUD_QUOTA_IN_MB');
        if ($quota !== false) {
            $this->quota = intval($quota);
        }
    }

    public function tryToCreate(object $userData)
    {
        global $strings;
        $userData->quota = $this->quota;
        $answer = $this->createAccount($userData);
        if ($answer->success === false) {
            sendAPIResponse(400, createAPIResponse("general", $strings[$answer->type]));
        }
    }

    public function validateData(object $userData): ValidatedData
    {
        $id = "e_cloud_account_data";
        try {
            // We check if account with uid set to email or username exists
            if ($this->isUsernameTaken($userData->username) || $this->isAliasTaken($userData->username)) {
                return new \ValidatedData($id, "error_account_taken");
            }
        } catch (\Error $_) {
            return new \ValidatedData($id, "error_server_side");
        }
        return new \ValidatedData($id, null);
    }

    protected function postCreationActions(object $userData, string $commonApiVersion = '')
    {
        try {
            $hmeAlias = '';
            
            $aliasDomain =  getenv('ALIAS_DOMAIN');
            // Create HME Alias
            $hmeAlias = $this->createHMEAlias($userData->email, $this->commonApiUrl, $commonApiVersion, $aliasDomain);
            // Create Alias to new domain
            // $this->createNewDomainAlias($username, $email, $commonApiUrl, $commonApiVersion, $aliasDomain);

            // Create alias with same name as email pointing to email to block this alias
            $domain = getMailDomain();
            $this->createNewDomainAlias($userData->username, $userData->email, $this->commonApiUrl, $commonApiVersion, $domain);
            $userData->hmeAlias = $hmeAlias;
        } catch (Error $e) {
            error_log('Error during alias creation for user: ' . $userData->username . ' with email: ' . $userData->email . ' : ' . $e->getMessage());
        }
        sleep(2);
        $userData->quota = strval($userData->quota) . ' MB';
        $answer = $this->setAccountDataAtNextcloud($userData);
        return $answer;
    }

    private function createHMEAlias(string $resultmail, string $commonApiUrl, string $commonApiVersion, string $domain) : string
    {
        $token = getenv('COMMON_SERVICES_TOKEN');
        $endpoint = $commonApiVersion . '/aliases/hide-my-email/';
        $url = $commonApiUrl . $endpoint . $resultmail;
        $data = array(
            "domain" => $domain
        );
        $headers = [
            "Authorization: Bearer $token"
        ];

        $result = curlRequest('POST', $url, $headers, $data);
        $output = $result->output;
        if ($result->statusCode != 200) {
            $err = $output->message;
            throw new Error($err);
        }
        $alias = isset($output->emailAlias) ? $output->emailAlias : '';
        return $alias;
    }

    private function createNewDomainAlias(string $alias, string $resultmail, string $commonApiUrl, string $commonApiVersion, string $domain)
    {
        $token = getenv('COMMON_SERVICES_TOKEN');

        $endpoint = $commonApiVersion . '/aliases/';
        $url = $commonApiUrl . $endpoint . $resultmail;

        $data = array(
            "alias" => $alias,
            "domain" => $domain
        );
        $headers = [
            "Authorization: Bearer $token"
        ];

        $result = curlRequest('POST', $url, $headers, $data);
        $output = $result->output;
        if ($result->statusCode != 200) {
            $err = $output->message;
            throw new Error($err);
        }
    }

    private function setAccountDataAtNextcloud(object $userData)
    {
        $token = getenv('ECLOUD_ACCOUNTS_SECRET');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        $data = [
            "token" => $token,
            "email" => $userData->email,
            "quota" => $userData->quota,
            "recoveryEmail" => $userData->authmail,
            "hmeAlias" => $userData->hmeAlias,
            "tosAccepted" => $userData->tosAccepted
        ];
        $data['uid'] = $this->usernameIsEmail ? $userData->email : $userData->username;
        $data['userLanguage'] = strtolower(getCurrentRequestLanguage());
        curl_setopt($ch, CURLOPT_URL, $this->ecloudAccountsApiUrl . 'set_account_data');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $output = curl_exec($ch);
        $output = json_decode($output, false);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $answer = new \stdClass;
        $answer->success = true;

        $errorNotEmpty = !empty($output->error);
        $isRecoveryEmailError = $errorNotEmpty && $output->error === 'error_setting_recovery';
        $isHmeError = $errorNotEmpty && $output->error === 'error_adding_hme_alias';

        if ($isRecoveryEmailError) {
            $message = 'Setting recovery email of user ' . $userData->email . ' failed with status code: ' . $statusCode . '(recovery email: ' . $userData->authmail . ')' . PHP_EOL;
            error_log($message, 0);
        }
        if ($isHmeError) {
            $message = 'Setting HME alias of user ' . $userData->email . ' failed with status code: ' . $statusCode . '(HME alias: ' . $userData->hmeAlias . ')' . PHP_EOL;
            error_log($message, 0);
        }

        if ($statusCode !== 200) {
            // Don't fail if recovery email or hide-my-email alias not set correctly
            $answer->success = $isRecoveryEmailError || $isHmeError;
            $answer->type = $errorNotEmpty ? $output->error : 'error_creating_account';
        }

        return $answer;
    }

    private function isUsernameTaken(string $uid): bool
    {
        $token = getenv('ECLOUD_ACCOUNTS_SECRET');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        $data = array(
            "uid" => $uid,
            "token" => $token,
        );
        curl_setopt($ch, CURLOPT_URL, $this->ecloudAccountsApiUrl . 'user_exists');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $output = curl_exec($ch);
        $output = json_decode($output);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($statusCode !== 200) {
            $err = curl_error($ch);
            throw new Error($err);
        }

        return $output;
    }

    private function isAliasTaken(string $alias): bool
    {
        $token = getenv('COMMON_SERVICES_TOKEN');

        $endpoint = "v2/aliases/hide-my-email?alias=$alias";
        $url = $this->commonApiUrl . $endpoint;
      
        $headers = [
            "Authorization: Bearer $token"
        ];

        $result = curlRequest('GET', $url, $headers);
        $statusCode = $result->statusCode;
        if ($statusCode !== 200 && $statusCode !== 404) {
            throw new Error("Error with request to check if alias exists, status code : " . $statusCode);
        }
        return $statusCode === 200;
    }

    protected function createAccount(object $userData)
    {
    }
}
