<?php
require 'vendor/autoload.php';
require_once('language.php');
require_once('helpers.php');
require_once('accounts/account_creator.php');
require_once('accounts/base_ecloud_account_creator.php');


use phpseclib3\Net\SSH2;

class ECloudAccountCreator extends BaseEcloudAccountCreator
{
    public function __construct(string $ecloudUrl)
    {
        parent::__construct($ecloudUrl);
    }

    protected function createAccount(object $userData)
    {
        global $strings;
        $PF_HOSTNAME = "postfixadmin";
        $PF_USER = "pfexec";
        $PF_PWD = getenv("POSTFIXADMIN_SSH_PASSWORD");

        $ssh = new SSH2($PF_HOSTNAME);
        if (!$ssh->login($PF_USER, $PF_PWD)) {
            $error_string = $strings["error_server_side"];
            sendAPIResponse(500, createAPIResponse("general", $error_string));
        }


        // 1 - create the account
        $creationFeedBack = explode("\n", $ssh->exec('/postfixadmin/scripts/postfixadmin-cli mailbox add ' . escapeshellarg($userData->email) . ' --password ' . escapeshellarg($userData->password) . ' --password2 ' . escapeshellarg($userData->password) . ' --name ' . escapeshellarg($userData->name) . ' --email_other ' . escapeshellarg($userData->authmail) . ' --quota ' . $userData->quota . ' --active 1 --welcome-mail 0 2>&1'));
        $isCreated = preg_grep('/added/', $creationFeedBack);
        $answer = new \stdClass();
        if (empty($isCreated)) {
            // There was an error during account creation on PFA side, return it
            $answer->success = false;
            $answer->type = "error_creating_account";
            return $answer;
        } else {
            $answer = $this->postCreationActions($userData);
            return $answer;
        }
    }

}
