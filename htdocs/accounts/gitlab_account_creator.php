<?php
require_once('accounts/account_creator.php');

class GitlabAccountCreator implements AccountCreator
{
    private string $gilabUrl;
    private string $gilabUsersUrl;
    private string $gitlabToken;
    public function __construct(string $gilabUrl, string $gitlabToken) {
        $this->gilabUrl = endsWith($gilabUrl, "/") ? $gilabUrl : $gilabUrl . "/";
        $this->gilabUsersUrl = $this->gilabUrl . "api/v4/users";
        $this->gitlabToken = $gitlabToken;
    }
    public function validateData(object $userData): ValidatedData
    {
        $id = "gitlab_account_data";
        if ($this->isUsernameTaken($userData->username)) return new \ValidatedData($id, "error_account_taken");
        return new \ValidatedData($id, null);
    }
    private function isUsernameTaken($username): bool {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->gilabUrl . "users/" . $username . "/exists",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
            ),
        ));
        $res = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if (!empty($err)) {
            throw new Error($err);
        }
        $res = json_decode($res, false);
        return $res->exists;
    }

    private function sendGitlabRequest(object $userData): object
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->gilabUsersUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($userData),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Bearer " . $this->gitlabToken
            ),
        ));
        $res = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        $err = curl_error($curl);

        curl_close($curl);

        $return = new \stdClass();
        $return->status=$statusCode;
        if (!empty($err)) {
            $return->message=$err;
            return $return;
        }
        if ($statusCode < 200 || $statusCode >=300) {
            $res = json_decode($res, false);
            $return->message = isset($res->message) ? $res->message : $res->error;
            return $return;
        }
        return $return;
    }

    public function tryToCreate(object $userData)
    {
        $response = $this->sendGitlabRequest($userData);
        if ($response->status < 200 || $response->status >=300) {
            throw new Exception($response->message);
        }
    }
}