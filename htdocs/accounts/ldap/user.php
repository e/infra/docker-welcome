<?php 

use LdapRecord\Models\Model;

class User extends Model {
    public static $objectClasses = [
        'murenaUser',
        'simpleSecurityObject'
    ];
};         