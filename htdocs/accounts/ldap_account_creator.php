<?php
require 'vendor/autoload.php';
require_once('accounts/account_creator.php');
require_once('accounts/base_ecloud_account_creator.php');
require_once('accounts/ldap/user.php');
require_once('language.php');
require_once('helpers.php');

use LdapRecord\Connection;
use LdapRecord\Container;

class LDAPAccountCreator extends BaseEcloudAccountCreator
{
    private Connection $conn;

    public function __construct(string $ecloudUrl)
    {
        parent::__construct($ecloudUrl);
        $this->usernameIsEmail = false;
        $this->connectToLDAPServer();
    }

    protected function createAccount(object $userData)
    {
        $baseDn =  getenv('LDAP_USERS_BASE_DN');
        $userDn = "username=$userData->username," . $baseDn;
        $userClusterID = getenv('CLUSTER_ID');
        $answer = new \stdClass();
        $quotaInBytes = $userData->quota * 1024 * 1024; # convert to bytes
        try {
            $user =[
                'mailAddress' => $userData->email,
                'username' => $userData->username,
                'usernameWithoutDomain' => $userData->username,
                'userPassword' => $userData->password,
                'displayName' => $userData->name,
                'quota' => $quotaInBytes,
                'mailAlternate' => $userData->authmail,
                'recoveryMailAddress' => $userData->authmail,
                'active'=> 'TRUE',
                'mailActive' => 'TRUE',
                'userClusterID' => $userClusterID,
                'objectClass' => User::$objectClasses
            ];
            $userEntry = new User($user);
            $userEntry->setDn($userDn);
            $userEntry->save();
        } catch (Exception $e) {
            error_log('Error creating user ' . $e->getMessage());
            $answer->success= false;
            $answer->type = 'error_creating_account';
            return $answer;
        }
        $answer = $this->postCreationActions($userData, 'v2');
        return $answer;
    }

    private function getLDAPConfig() : array
    {
        $ldapHosts = getenv('LDAP_HOSTS');
        $ldapHosts = explode(",", $ldapHosts);
        $ldapPort = getenv('LDAP_PORT');
        $ldapAdminDn = getenv('LDAP_ADMIN_DN');
        $ldapAdminPassword = getenv('LDAP_ADMIN_PASSWORD');

        $baseDn = getenv('LDAP_USERS_BASE_DN');

        return [
          'hosts' => $ldapHosts,
          'port' => $ldapPort,
          'base_dn' =>   $baseDn,
          'username' => $ldapAdminDn,
          'password' => $ldapAdminPassword
        ];
    }

    private function connectToLDAPServer() : void
    {
        $config = $this->getLDAPConfig();

        $this->conn = new Connection(
            $config
        );
        Container::addConnection($this->conn);
    }
}
