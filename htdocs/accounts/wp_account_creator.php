<?php
require_once('accounts/account_creator.php');

class WPAccountCreator implements AccountCreator
{
    private string $wordPressUrl;
    private string $wordPressCredentials;
    private string $wordPressUserUrl;
    public function __construct(string $wordPressUrl, string $username, string $appPassword, string $shopLocation = null)
    {
        $this->wordPressUrl = endsWith($wordPressUrl, "/") ? $wordPressUrl : $wordPressUrl . "/";
        $this->wordPressUserUrl = $this->wordPressUrl . "?rest_route=/wp/v2/users";
        if ($shopLocation) {
            $this->wordPressUrl .= $shopLocation . '/';
        }
        $this->wordPressCredentials = base64_encode($username . ":" . $appPassword);
    }
    public function validateData(object $userData): ValidatedData
    {
        $username = $this->createUsernameFromEmail($userData->email);
        $id = "WP_account_data";
        if ($this->isUsernameTaken($username)) return new \ValidatedData($id, "error_account_taken");
        if ($this->isEmailTaken($userData->email)) return new \ValidatedData($id, "error_already_registered");
        return new \ValidatedData($id, null);
    }

    private function createUsernameFromEmail(string $email): string {
        $replacer = array(
            "@" => "",
            "." => "-",
        );
        return strtr($email, $replacer);
    }

    private function isUsernameTaken($username): bool
    {
        $users = $this->getUsersBySearchString($username);
        if (empty($users)) return false;

        foreach ($users as $user) {
            if ($user->slug === $username) {
                return true;
            }
        }
        return false;
    }

    private function isEmailTaken(string $email): bool
    {
        $users = $this->getUsersBySearchString($email);
        // Undocumented behavior at https://developer.wordpress.org/rest-api/reference/users/#example-request :
        // When the search param has at least one "@", WP search exclusively on the email field.
        // i.e. if at least one user is found, it means that this user has the email that we searched
        return !empty($users);
    }

    private function getUsersBySearchString(string $searchTerm): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->wordPressUserUrl . "&search=" . $searchTerm,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Basic " . $this->wordPressCredentials
            ),
        ));
        $res = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if (!empty($err)) {
            throw new Error($err);
        }
        $users = json_decode($res);
        return $users;
    }

    private function getRefCodeCookie(object $userData) {
        $url = $this->wordPressUrl;
        $url .= "?&ref=" . $userData->referrerCode;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HEADER => 1,
        ));
        $res = curl_exec($curl);
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi',
          $res,  $match_found);
        $cookies = array();
        foreach($match_found[1] as $item) {
            parse_str($item,  $cookie);
            $cookies = array_merge($cookies,  $cookie);
        }
        return $cookies["mwb_cpr_cookie_set"];
   
    }

    public function sendWPRewardRequest(object $userData): bool
    {
        if ($userData->referrerCode == null) {
            return false;
        }
        $curl = curl_init();
        $url = $this->wordPressUrl . "?rest_route=/referral-program/v1/reward-referrer";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(array(
                "ref_code" => $userData->referrerCode,
                "ecloud_username" => $userData->email,
            )),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Basic " . $this->wordPressCredentials                
            ),
        ));
        $res = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        $err = curl_error($curl);
        if (!empty($err) || $statusCode != 200) {
            return false;
        }
        return true;
    }
            

    private function sendWPRequest(object $userData): object
    {
        $curl = curl_init();
        $cookie = null;
        $url = $this->wordPressUserUrl;
        if ($userData->referrerCode != null) {
            $url .= "&ref=" . $userData->referrerCode;
            $cookie = $this->getRefCodeCookie($userData);
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($userData),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "Authorization: Basic " . $this->wordPressCredentials,
                "Cookie: mwb_cpr_cookie_set=$cookie"
            ),
        ));
        $res = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        $err = curl_error($curl);

        $return = new \stdClass();
        $return->status=$statusCode;
        if (!empty($err)) {
            $return->message=$err;
            return $return;
        }
        if ($statusCode < 200 || $statusCode >=300) {
            $res = json_decode($res, false);
            $return->message = $res->message;
            return $return;
        }
        return $return;
    }

    public function tryToCreate(object $userData)
    {
        $userData->username = $this->createUsernameFromEmail($userData->email);
        $response = $this->sendWPRequest($userData);
        if ($response->status < 200 || $response->status >=300) {
            throw new Exception($response->message);
        }
    }
}