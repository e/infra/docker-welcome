<?php session_start(); ?>

<?php
if (isset($_POST['submitform'])) {
    $secure = isset($_POST['secure']) ? strtolower($_POST['secure']) : '';
    if ($secure == $_SESSION['securecode']) {
        echo 'Le code de sécurité est ok';
        unset($_SESSION['securecode']);
    } else {
        echo 'Le code de sécurité est incorrecte!';
        unset($_SESSION['securecode']);
        flush();
        ob_flush();
        sleep(1);
        header("Location: https://e.foundation/test.php");
    }
}
?>