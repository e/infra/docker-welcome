<?php
require 'vendor/autoload.php';
require_once('language.php');

$DISPOSABLE_DOMAINS = json_decode(file_get_contents("/var/accounts/disposable_domains.json"));

function createAPIResponse($type, $message)
{
    $object_message = new \stdClass();
    $object_message->type = $type;
    $object_message->message = $message;
    return $object_message;
}

function sendAPIResponse($response_code, $message)
{
    http_response_code($response_code);
    respond_with_json($message);
}

function curlRequest(string $method, string $url, array $headers = [], array $data = []) : stdClass
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

    curl_setopt($ch, CURLOPT_URL, $url);
    if (!empty($headers)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }

    if ('POST' === $method && !empty($data)) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    }
    
    $output = curl_exec($ch);
    $output = json_decode($output, false);
    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    $result = new stdClass();
    $result->statusCode = $statusCode;
    $result->output = $output;
    return $result;
}

function get_easy_installer_message($code)
{
    $ret = array(
        'errcode' => $code
    );
    return $ret;
}

function isEmailDomainBlackListed($email): bool
{
    global $DISPOSABLE_DOMAINS;
    $domain = substr($email, strrpos($email, '@') + 1, strlen($email));
    $blacklisted = array();
    if (file_exists("/var/accounts/blacklist")) {
        $content = file_get_contents("/var/accounts/blacklist");
        $blacklisted = explode("\n", $content);
    }
    return in_array($domain, $blacklisted) || in_array($domain, $DISPOSABLE_DOMAINS);
}

function sendInviteMail($to, $secret, $lang, ?string $referrer = null, ?string $shopLocation = null)
{
    $encoded_email = urlencode($to);
    $domain = getenv("DOMAIN");
    $signupURL = "https://$domain/apps/ecloud-accounts/accounts/";
    if ($lang != "en") {
        $signupURL .= "$lang/";
    }
    $signupURL .= "signup?recoveryEmail=$encoded_email";
    if ($referrer != null) {
        $signupURL .= "&ref=$referrer";
    }
    if ($shopLocation) {
        $signupURL .= "&shop=$shopLocation";
    }
    $SENDGRID_API_KEY = getenv("SENDGRID_API_KEY");
    if (!empty($SENDGRID_API_KEY)) {
        return sendInviteMailWithSendGrid($to, $signupURL);
    }
    return sendInviteMailWithPHPMail($to, $signupURL, $lang);
}

function sendInviteMailWithPHPMail(string $to, string $signupURL, string $lang): bool
{
    global $strings;
    $template = null;
    if (!check_if_lang_exists($lang)) {
        $lang = "en";
    }
    $subject = $strings['email_confirmation_subject'];
    $from = getenv("WELCOME_MAIL_FROM") ? getenv("WELCOME_MAIL_FROM") : getenv("WELCOME_SMTP_FROM");
    $headers = ['From' => $from, 'To' => $to, 'Subject' => $subject,
    'Content-Type' => 'text/html; charset=UTF-8', 'Date' => date('D, d M Y H:i:s O')];

    // include text and HTML versions
    $text = file_get_contents("/var/www/html/invite_template/email_confirmation_$lang.txt");
    $html = file_get_contents("/var/www/html/invite_template/email_confirmation_$lang.html");

    $text = str_replace("{{TARGETURL}}", $signupURL, $text);
    $html = str_replace("{{TARGETURL}}", $signupURL, $html);


    $mime = new Mail_mime(
        array(
            "head_charset" => "utf-8",
            "text_charset" => "utf-8",
            "html_charset" => "utf-8",
            "eol" => "\n"
        )
    );
    $mime->setTXTBody($text);
    $mime->setHTMLBody($html);

    $body = $mime->get();
    $headers = $mime->headers($headers);

    $host = getenv("SMTP_HOST");
    $username = getenv("WELCOME_SMTP_FROM");
    $password = getenv("WELCOME_SMTP_PW");
    $port = getenv("SMTP_PORT");


    $smtp = Mail::factory('smtp', [
        'host' => $host,
        'auth' => true,
        'username' => $username,
        'password' => $password,
        'port' => $port
    ]);

    $mail = $smtp->send($to, $headers, $body);

    if (PEAR::isError($mail)) {
        return false;
    } else {
        return true;
    }
}

function sendInviteMailWithSendGrid(string $to, string $signupURL): bool
{
    $lang = strtoupper(getCurrentRequestLanguage());

    $TEMPLATE_ID = getLocalizedEnv("SENDGRID_TEMPLATE_ID_EMAIL_CONFIRMATION", $lang);

    $substitutions = [
      new \SendGrid\Mail\Substitution("TARGETURL", $signupURL),
    ];

    $email = createSendGridEmail($to, $TEMPLATE_ID, $substitutions);

    return sendEmailWithSendGrid($email);
}

/**
 * @return string valid language code
 */
function getCurrentRequestLanguage(): string
{
    if (!isset($_GET["lang"])) {
        return "en";
    }
    $lang = strtolower($_GET["lang"]);
    return check_if_lang_exists($lang) ? $lang : "en";
}

/**
 * @return string a value from getenv but localized by language. Tries to catch at least the value for the default lang or throws an error
 * <code>
 * <?php
 * echo getLocalizedEnv("HELLO", "fr"); // Bonjour
 * echo getLocalizedEnv("HELLO", "en"); // Hello
 * echo getLocalizedEnv("WORLD", "does_not_exist"); // World
 * echo getLocalizedEnv("ONLY_IN_ENG", "fr"); // Only in english
 * echo getLocalizedEnv("DOES_NOT_EXIST", "en"); // throw an error
 * ?>
 * </code>
 */
function getLocalizedEnv(string $envKey, string $lang): string
{
    $lang = strtolower($lang);
    if (!check_if_lang_exists($lang)) {
        $lang = "en";
    }

    $lang = strtoupper($lang);

    $value = getenv("{$envKey}_{$lang}");
    if (!empty($value)) {
        return $value;
    }
    // Searching for default value
    $value = getenv("{$envKey}_EN");
    if (empty($value)) {
        throw new Exception("ENV variable '$envKey' was not found for language '$lang' nor a default value was set");
    }
    return $value;
}

function createSendGridEmail(string $to, string $templateId, ?array $substitutions, ?int $delay = 0): \SendGrid\Mail\Mail
{
    $SEND_FROM = getenv("SENDGRID_EMAIL_FROM");
    if ($SEND_FROM === false) {
        throw new Exception("SENDGRID_EMAIL_FROM env variable is required to send email with SendGrid");
    }

    $email = new \SendGrid\Mail\Mail();
    $email->setFrom($SEND_FROM);
    $email->addTo($to);

    if ($substitutions !== null) {
        $email->addDynamicTemplateDatas($substitutions);
    }
    $email->setTemplateId($templateId); 
    $email->setSendAt($delay); // send mail after $delay
    return $email;
}

function sendEmailWithSendGrid(\SendGrid\Mail\Mail $email): bool
{
    $SENDGRID_API_KEY = getenv("SENDGRID_API_KEY");
    if ($SENDGRID_API_KEY === false) {
        throw new Exception("SENDGRID_API_KEY env variable is required to send email with SendGrid");
    }
    $sendgrid = new \SendGrid($SENDGRID_API_KEY);
    try {
        $response = $sendgrid->send($email);
        return $response->statusCode() == 202;
    } catch (Exception $e) {
        echo 'Caught exception: '. $e->getMessage() ."\n";
        return false;
    }
}

function respond_with_json($response)
{
    header('Content-type: application/json');
    exit(json_encode($response));
}

function check_if_lang_exists($lang): bool
{
    $available_langs = array("en", "de", "fr", "it", "es");
    return in_array($lang, $available_langs);
}

function startsWith($haystack = "", $needle)
{
    $length = strlen($needle);
    return $length > 0 ? (substr($haystack, 0, $length) === $needle) : 0;
}

function endsWith(string $string, string $endString): bool
{
    $len = strlen($endString);
    if ($len == 0) {
        return true;
    }
    return (substr($string, -$len) === $endString);
}

function authUsed($authstr)
{
    clearstatcache();
    if (file_exists("/var/accounts/auth.file.done")) {
        $handle = fopen("/var/accounts/auth.file.done", "r");
        while (($line = fgets($handle)) !== false) {
            if (startsWith($line, $authstr)) {
                $account = explode(":", $line);
                return trim($account[2]);
            }
        }
        fclose($handle);
        return;
    } else {
        return;
    }
}

function isAuthorized($mail, $secret)
{
    $handle = fopen("/var/accounts/auth.file", "r");
    $res = new \stdClass();
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            if (strcmp(trim($line), "$mail:$secret") == 0) {
                $account = authUsed(trim($line));
                if (empty($account)) {
                    $res->success = true;
                    return $res;
                } else {
                    $res->account = $account;
                    $res->success = false;
                    return $res;
                }
            }
        }
        fclose($handle);
    } else {
        $res->success = false;
        return $res;
    }
    $res->success = false;
    return $res;
}

// FIXME: to remove, supporting only $DOMAIN
function getMailDomain(): string
{
    $mailDomain = getenv("MAIL_DOMAIN");
    if (empty($mailDomain)) {
        return getenv("DOMAIN");
    }
    return $mailDomain;
}

function hasEmailAlreadyCreatedAnAccount(string $email): bool
{
    $AUTH_FILE_DONE = "/var/accounts/auth.file.done";
    return extractCredentialsFromAuthFile($AUTH_FILE_DONE, $email) !== null;
}

function hasRegistrationPendencyForEmail(string $email): bool
{
    return getCredentialsFromPendingByEmail($email) !== null;
}

function getCredentialsFromPendingByEmail(string $email): ?array
{
    $AUTH_FILE = "/var/accounts/auth.file";
    return extractCredentialsFromAuthFile($AUTH_FILE, $email);
}

function extractCredentialsFromAuthFile(string $authFilePath, string $email): ?array
{
    $fileContents = file_get_contents($authFilePath);
    $separator = "\n";

    $line = strtok($fileContents, $separator);

    while ($line !== false) {
        $credentials = explode(':', $line);
        if ($credentials[0] === $email) {
            return $credentials;
        }
        $line = strtok($separator);
    }
    return null;
}
