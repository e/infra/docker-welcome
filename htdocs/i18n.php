<?php
$available_langs = array("en", "de", "fr", "it", "es");
if (isset($_GET["lang"])) {
    $lang = $_GET["lang"];
    if (!in_array($lang, $available_langs)) {
        $lang = "en";
    }
} else {
    $lang = "en";
}

$parsed = yaml_parse_file("./_i18n/$lang.yml");

$domain = getenv("DOMAIN");
$mail_domain = getenv("MAIL_DOMAIN");
$parsed["domain"] = $domain;
if (!empty($mail_domain)) {
    $parsed["mail_domain"] = $mail_domain;
} else {
    $parsed["mail_domain"] = $domain;
}
header("Content-Type: application/json");
echo json_encode($parsed);
