<?php
$lang = "en";
$available_langs = array("en", "de", "fr", "it", "es");
$strings = null;

if (isset($_GET['lang'])) {
    $lang = $_GET['lang'];
} elseif (isset($_POST['trp-form-language'])) {
    $lang = $_POST["trp-form-language"];
} else {
    if(!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
}

if (!in_array($lang, $available_langs) || !file_exists("./_i18n/$lang.yml")) {
    $lang = "en";
}
$strings = yaml_parse_file("./_i18n/$lang.yml");
