<?php

require '/var/www/html/vendor/autoload.php';

use phpseclib3\Net\SSH2;

/**
 * function to purge system files on NC account deletion
 * for users to be able to re-register after account deletion
 * with same origin email
 *
 */
function purgeAccountFiles()
{

    // system files maintaining registration informations.
    $AUTH_FILE_DONE = "/var/accounts/auth.file.done";
    $AUTH_FILE = "/var/accounts/auth.file";

    /**
     * for $AUTH_FILE_DONE, line pattern is :
     * MAIL_USED_FOR_REGISTRATION:SECRET:$userOnly
     *
     * remove UNIQUE line on this file based on $userOnly
     *
     * create regex pattern to prevent false positives :
     * $userOnly is 'john'
     * we don't want to find lines with john_doe, bigjohn, johnny,...
     *
     */
    global $userOnly;
    $regex = "/:" . preg_quote($userOnly) . "$/i";

    $lockedFileDone = fopen($AUTH_FILE_DONE, "c", LOCK_EX);
    // c mode to open the file in write mode WITH EXCLUSIVE LOCK, but DO NOT truncate it

    // find and delete the line
    $lines = file($AUTH_FILE_DONE, FILE_IGNORE_NEW_LINES);
    foreach ($lines as $key => $line) {
        if (preg_match($regex, $line) == 1) {
            // temporarely save the line for later use on the file below
            $tmpLine = $line;

            unset($lines[$key]);
        }
    }
    if ($tmpLine) {
        //Unique line was found, save $AUTH_FILE_DONE with exclusive lock on the file
        $lines[] = "";
        $data = implode(PHP_EOL, $lines);
        ftruncate($lockedFileDone, 0);
        fwrite($lockedFileDone, $data);
        fclose($lockedFileDone);

        /**
         * for $AUTH_FILE, line pattern is :
         * MAIL_USED_FOR_REGISTRATION:SECRET
         *
         * remove ALL lines on this file based on MAIL_USED_FOR_REGISTRATION
         *
         * get MAIL_USED_FOR_REGISTRATION from $tmpLine stored earlier
         * create regex pattern to prevent false positives :only lines STARTING with $mail
         */
        $mail = strtok($tmpLine, ":");
        $regex = "/^" . preg_quote($mail) . ":/";

        $lockedFile = fopen($AUTH_FILE, "c", LOCK_EX);
        // c mode to open the file in write mode WITH EXCLUSIVE LOCK, but DO NOT truncate it

        // find and delete all the line containing this MAIL_USED_FOR_REGISTRATION
        $lines = file($AUTH_FILE, FILE_IGNORE_NEW_LINES);
        foreach ($lines as $key => $line) {
            if (preg_match($regex, $line) == 1) {
                unset($lines[$key]);
            }
        }
        $lines[] = "";
        $data = implode(PHP_EOL, $lines);
        //save $AUTH_FILE with exclusive lock on the file
        ftruncate($lockedFile, 0);
        fwrite($lockedFile, $data);
        fclose($lockedFile);

        // return MAIL_USED_FOR_REGISTRATION
        return $mail;
    } else {
        return null;
    } //NO line was found for this user
}

/**
 * function to  :
 * - connect to postfixadmin container to delete user account, using postfixadmin-cli
 * - delete account's maildir as mail  volume is now bind mounted to PFA container too
 *
 */
function deleteMailAccount()
{
    $PF_HOSTNAME = "postfixadmin";
    $PF_USER = "pfexec";
    $PF_PWD = getenv("POSTFIXADMIN_SSH_PASSWORD");

    // Dir where /mnt/repo-base/volumes/mail/ is bind mounted on postfixadmin container
    $baseDir = "/var/mail/vhosts/";

    global $user2delete, $userOnly, $domain;

    if (!empty($domain) && !empty($userOnly)) {
        $ssh = new SSH2($PF_HOSTNAME);
        if (!$ssh->login($PF_USER, $PF_PWD)) {
            exit('Login Failed');
        }

        $ssh->exec('/postfixadmin/scripts/postfixadmin-cli mailbox delete ' . escapeshellarg($user2delete));

        $ssh->exec('sudo /usr/local/bin/postfixadmin-mailbox-postdeletion.sh ' . escapeshellarg($userOnly) . ' ' . escapeshellarg($domain));

        $aliases2delete = getUserAliases();
        foreach ($aliases2delete as $alias) {
            $feedback = explode('\n', $ssh->exec('/postfixadmin/scripts/postfixadmin-cli alias delete ' . escapeshellarg($alias)));
            $isDeleted = preg_grep('/deleted/', $feedback);
            if (!$isDeleted) {
                error_log('Error deleting alias '. $alias . ' for user ' . $user2delete);
            }
        }
        // verify it's done
        $delDbConfirm = $ssh->exec('/postfixadmin/scripts/postfixadmin-cli mailbox view ' . escapeshellarg($user2delete) . ' 2>&1 |grep "not valid"');

        // build path to check deletion
        $fullPath = $baseDir . $domain . "/" . $userOnly;
        $delDirConfirm = $ssh->exec('[ ! -d ' . escapeshellarg($fullPath) . ' ] && echo "DELETED"');

        if (($delDbConfirm === "Error: The EMAIL is not valid!") && ($delDirConfirm === "DELETED")) {
            return true;
        } else {
            return false;
        } // one of the deletion did not go well!
    } else {
        return null;
    } // $domain OR $userOnly empty, do nothing!!
}

function getUserAliases()
{
    global $user2delete;

    $dbUser = getenv('PFDB_USR');
    $password = getenv('PFDB_PW');
    $db = getenv('PFDB_DB');
    $host = getenv("PFDB_HOST");

    $mysqli = new mysqli($host, $dbUser, $password, $db);
    $stmt = $mysqli->prepare('SELECT address FROM alias where goto= ?');
    $stmt->bind_param('s', $user2delete);
    $stmt->execute();

    $result = $stmt->get_result();
    $aliases = $result->fetch_all(MYSQLI_ASSOC);
    $aliases = array_map(fn ($entry) => $entry['address'], $aliases);
    return $aliases;
}

if (sha1($_POST['sec']) !== getenv("WELCOME_SECRET_SHA")) {
    http_response_code(403);
    exit();
} else {
    $user2delete = $_POST['uid'];
    $exploded = explode("@", $user2delete);
    $userOnly = $exploded[0];
    $domain = $exploded[1];

    // STEP 1 : remove $user2delete from postfix database AND remove its mail folder
    $mailDeletionReturn = deleteMailAccount();
    if ($mailDeletionReturn == true) {
        /**
         * mail DB account AND mailbox dir successfully deleted
         * NO user data remaining on the server
         * TODO :
         * - fire mail for user to confirm deletion of his account is complete
         * - handle onlyoffice part
         *
         */
    }
    // STEP 2 : Purge system files AUTH_FILE & AUTH_FILE_DONE
    $registrationMail = purgeAccountFiles();
    return ($registrationMail !== null);
}
