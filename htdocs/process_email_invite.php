<?php
require 'vendor/autoload.php';
require_once('language.php');
require_once('helpers.php');

$domain = getenv("DOMAIN");
$mail_domain = getenv("MAIL_DOMAIN");

if (empty($mail_domain)) {
    $mail_domain = $domain;
}

$easy_installer_uas = array("Java 11 HttpClient Bot");
$ua = $_SERVER['HTTP_USER_AGENT'];
$ua_from_ei = in_array($ua, $easy_installer_uas);
$check_exists = isset($_POST["check"]);

if ($ua_from_ei) {
    if (!$check_exists) {
        $ret = get_easy_installer_message(400);
        respond_with_json($ret);
    }
}

$from_easy_installer = $check_exists && $ua_from_ei;

if (isAccountCreationDisabled()) {
    if ($from_easy_installer) {
        $ret = get_easy_installer_message(600);
        respond_with_json($ret);
    }
    global $strings;
    $message = $strings['error_internal_registration'];
    $result = createAPIResponse("error", $message);
    http_response_code(503);
    respond_with_json($result);
}

session_set_cookie_params(['SameSite' => 'None', 'Secure' => true]);
session_start();
header("Access-Control-Allow-Origin: *");
$email1 = "";
$email2 = "";
$referrerCode = isset($_POST['ref']) ? $_POST['ref'] : null;
$referrerCode = is_string($referrerCode) ? $referrerCode : null;
$shopLocation = isset($_POST['shop']) ? $_POST['shop'] : null;

if ($from_easy_installer) {
    $email1 = trim(strtolower(htmlspecialchars($_POST["email"])));
    $email2 = $email1;
    if (!isset($_POST["email"])) {
        $ret = get_easy_installer_message(400);
        respond_with_json($ret);
    }
}
else {
    $email1 = trim(strtolower(htmlspecialchars($_POST["mail1"])));
    $email2 = trim(strtolower(htmlspecialchars($_POST["mail2"])));
}

if (shouldCheckCaptcha()) {
    captcha_check($email1, $from_easy_installer);
}
email_check($email1, $email2, $from_easy_installer);

unset($_SESSION['securecode']);

$result = new \stdClass();
if ( hasEmailAlreadyCreatedAnAccount($email1) ) {
    $result->type = "general";
    $result->key = "error_already_registered";
    if ($from_easy_installer) {
        $ret = get_easy_installer_message(200);
        respond_with_json($ret);
    }
    respond_with_message(400, $result, false, $email1);
    exit();
}

// Create a 16 character long random auth secret
$unique_key = bin2hex(random_bytes(8));

$hasRegistrationPendency = hasRegistrationPendencyForEmail($email1);
if ( $hasRegistrationPendency ) {
    $unique_key = getCredentialsFromPendingByEmail($email1)[1];
}
$sent = sendInviteMail($email1, $unique_key, $lang, $referrerCode, $shopLocation);
if (!$sent) {
    if ($from_easy_installer) {
        $ret = get_easy_installer_message(500);
        respond_with_json($ret);
    }
    $result->type = "general";
    $result->key = "error_internal_registration";
    respond_with_message(400, $result, false, $email1);
    exit();
}

if ( !$hasRegistrationPendency ) {
    createRegistrationPendency($email1, $unique_key);
}

if ($from_easy_installer) {
    $ret = get_easy_installer_message(100);
    respond_with_json($ret);
}
$result->type = "success";
$result->key = 'success_registration_link';
respond_with_message(200, $result, true, $email1);

function createRegistrationPendency(string $email, string $uniqueKey)
{
    $auth_file_in_append_mode = fopen("/var/accounts/auth.file", "a");
    $to_append = "$email:$uniqueKey\n";
    fwrite($auth_file_in_append_mode, $to_append);
    fclose($auth_file_in_append_mode);
}

function respond_with_message($code, $message, $success, $email, $subs = null)
{
    global $strings;
    global $lang;
    global $referrerCode;
    global $shopLocation;
    $referer = $_SERVER['HTTP_REFERER'];
    global $domain;
    $referer_expected = getenv("REFERER");
    global $mail_domain;
    if ((strlen(stristr($referer, $referer_expected)) > 0)) {
        http_response_code(302);
        $encoded_email = urlencode($email);
        if ($lang == "en") {
            $redirect_url = "https://$domain/apps/ecloud-accounts/accounts/signup/?recoveryEmail=$encoded_email";
        } else {
            $redirect_url = "https://$domain/apps/ecloud-accounts/accounts/signup/$lang/?recoveryEmail=$encoded_email";
        }

        if ($referrerCode != null) {
            $redirect_url .= "&ref=$referrerCode";
        }
        if ($shopLocation != null) {
            $redirect_url .= "&shop=$shopLocation";
        }
        exit(header("Location: $redirect_url"));
    } else {
        http_response_code($code);
        $result_message = $strings[$message->key];
        if (!empty($subs)) {
            foreach ($subs as $key => $sub) {
                $result_message = str_replace($key, $sub, $result_message);
            }
        }
        $result = createAPIResponse($message->type, $result_message);
        respond_with_json($result);
    }
}

function email_check($email1, $email2, $from_easy_installer)
{
    $result = new \stdClass();
    $isEmailInvalid = ($email1 != $email2) || !isValidEmail($email1);

    if ($isEmailInvalid) {
        if ($from_easy_installer) {
            $ret = get_easy_installer_message(300);
            respond_with_json($ret);
        }
        unset($_SESSION['securecode']);
        $result->type = "general";
        $result->key = "error_email_not_identical";
        respond_with_message(400, $result, false, $email1);
    }
    if (isEmailDomainBlackListed($email1)) {
        if ($from_easy_installer) {
            $ret = get_easy_installer_message(300);
            respond_with_json($ret);
        }
        unset($_SESSION['securecode']);
        $result->type = "general";
        $result->key = "error_blacklisted_domain";
        $domain_name = substr(strrchr($email1, "@"), 1);
        $subs = array("@@@email_suffix@@@" => $domain_name);
        respond_with_message(400, $result, false, $email1, $subs);
    }
}

function captcha_check($email, $from_easy_installer)
{
    if ($from_easy_installer) {
        // Get array of secrets and filter empty
        $ei_secrets = array_filter(explode(',', getenv("EI_SECRET")));
        $to_check = htmlspecialchars($_POST["check"]);
        foreach ($ei_secrets as $secret) {
            $check = md5($email . trim($secret));
            if ($to_check === $check) {
                return;
            }
        }
        $ret = get_easy_installer_message(500);
        respond_with_json($ret);
    } else {
        $result = new \stdClass();
        $secure = isset($_POST['secure']) ? $_POST['secure'] : '';
        $secure = is_string($secure) ? strtolower($secure) : '';

        $isSessionCodeSet = isset($_SESSION['securecode']);
        $isSecureCodeRight = $secure == $_SESSION['securecode'];

        if (!($isSessionCodeSet && $isSecureCodeRight)) {
            unset($_SESSION['securecode']);
            $result->type = "secure_code";
            $result->key = "error_secure_code";
            respond_with_message(400, $result, false, $email);
        }
    }
}

function isValidEmail(string $email): bool {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}

function isAccountCreationDisabled(): bool {
    return getenv("CREATION_DISABLED") == 1;
}

function shouldCheckCaptcha(): bool {
    $bypassCaptchaToken = getenv("BYPASS_CAPTCHA_TOKEN");
    if (!isset($_GET['bypass_captcha_token']) || $bypassCaptchaToken === false) {
        return true;
    }

    $token = is_string($_GET['bypass_captcha_token']) ? $_GET['bypass_captcha_token'] : '';
    return $token !== $bypassCaptchaToken;
}
