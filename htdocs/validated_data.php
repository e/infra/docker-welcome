<?php

class ValidatedData
{
    private ?string $errorCode = null;
    private string $id = "";
    public function __construct(string $id, ?string $errorCode)
    {
        $this->id = $id;
        if ($errorCode !== null)
            $this->errorCode = $errorCode;
    }
    public function isValid(): bool
    {
        return $this->errorCode === null;
    }
    public function getErrorCode(): string
    {
        if ($this->errorCode === null)
            throw new RuntimeException("The data '{$this->id}' did not have any errors. Please check for errors using :isValid()");
        return $this->errorCode;
    }
}