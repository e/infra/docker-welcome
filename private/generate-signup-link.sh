#!/bin/bash
set -e

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
  echo "Usage: $(basename $0) -- Creates a new signup link
  options:
  --user-email  Pass the email address for the new user, so there is no need to prompt for it
  --lang Pass the preferred language of the user
  --help        Show this help
  example:
  ./generate-signup-link --lang es --user-email example@example-email.com
  "
  exit 0
fi

if [[ "$1" == "--user-email" ]]; then
  EMAIL="$2"
elif [[ "$3" == "--user-email" ]]; then
  EMAIL="$4"
else
  echo "What is the new user's current email address?"
  read EMAIL
fi

LANG="en"

if [[ "$1" == "--lang" ]]; then
  LANG="$2"
elif [[ "$3" == "--lang" ]]; then
  LANG="$4"
fi

export DOMAIN
export WELCOME_SMTP_FROM
export WELCOME_SMTP_PW
export SIGNUP_URL
export SIGNUP_RECIPIENT=$EMAIL
export LANG=$LANG
php /var/private/sendmail.php