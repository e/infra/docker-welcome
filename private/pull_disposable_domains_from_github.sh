#!/bin/bash
set -e

echo `date`
echo "Start sync of domains..."
curl https://raw.githubusercontent.com/disposable/disposable-email-domains/master/domains.json -o /var/accounts/disposable_domains.json
echo `date`
echo "Disposable domains synced at /var/accounts/disposable_domains.json..."
