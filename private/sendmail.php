<?php
require "/var/www/html/vendor/autoload.php";
require_once("/var/www/html/helpers.php");

$to = getenv("SIGNUP_RECIPIENT");
$signup_url = getenv("SIGNUP_URL");
$ENCODED_EMAIL = urlencode($to);
$DOMAIN = getenv("DOMAIN");
$AUTH_SECRET = $unique_key = uniqid();
$lang = getenv("LANG");

$auth_file_in_append_mode = fopen("/var/accounts/auth.file", "a");
fwrite($auth_file_in_append_mode, "$to:$AUTH_SECRET\n");
fclose($auth_file_in_append_mode);
if (!check_if_lang_exists($lang)) {
    $lang = "en";
}
$SIGNUP_URL = "";
$lang == "en" ? $SIGNUP_URL = "https://$DOMAIN/apps/ecloud-accounts/accounts/signup?recoveryEmail=$ENCODED_EMAIL"  : $SIGNUP_URL = "https://$DOMAIN/apps/ecloud-accounts/accounts/$lang/signup?recoveryEmail=$ENCODED_EMAIL";
echo "The new user can sign up now at $SIGNUP_URL\n";

$result = sendInviteMail($to, $AUTH_SECRET, $lang);
if ($result) {
    echo "Email Invite sent successfully\n";
} else {
    echo "Error sending email\n";
}
